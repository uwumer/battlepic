import argparse
import os.path
import sys

import cv2
import numpy

def errprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def die(code, *args, **kwargs):
    errprint(*args, **kwargs)
    exit(code)

def hex2rgb(hex: str) -> (int, int, int):
    color = bytes.fromhex(hex)
    if len(color) != 3:
        raise argparse.ArgumentTypeError('Color string must be formatted as RRGGBB')
    return color

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i',
        dest='image_path',
        required=True,
        metavar='IMAGE',
        type=str,
    )
    parser.add_argument(
        '-c',
        dest='color',
        default='DDDDDD',
        required=False,
        metavar='HEXRGB',
        type=lambda h: hex2rgb(h),
    )
    parser.add_argument(
        '-l',
        dest='cascade_path',
        default='cascade.xml',
        required=False,
        metavar='CASCADE',
        type=str,
    )
    parser.add_argument(
        '-o',
        dest='output_path',
        required=True,
        metavar='OUTPUT',
        type=str,
    )
    return parser.parse_args()

def draw_clouds(image, faces):
    image_height, image_width, _ = image.shape

    cv2.ellipse(
        image,
        (image_width, image_height//2),
        (image_width//6, image_height//2),
        0,
        0,
        360,
        args.color,
        -1,
    )

    for (x, y, w, h) in faces:
        triangle = numpy.array(
            [
                (x+w//2, y+h*3//4),
                (image_width, image_height//4),
                (image_width, image_height - image_height//4),
            ],
        )
        cv2.drawContours(
            image,
            [triangle],
            0,
            args.color,
            -1,
        )

def get_image(path):
    image = cv2.imread(path, cv2.IMREAD_UNCHANGED)
    if image is None:
        raise IOError
    return image

def get_faces(image, classifier):
    image_grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return classifier.detectMultiScale(image_grayscale, 1.1, 4)

def get_classifier(path):
    classifier = cv2.CascadeClassifier()
    if not classifier.load(path):
        raise IOError
    return classifier

def write_image(image, path):
    cv2.imwrite(path, image)

if __name__ == '__main__':
    args = parse_args()
    try:
        image = get_image(args.image_path)
    except IOError:
        die(1, 'Image file not found or its format is not supported')

    try:
        classifier = get_classifier(args.cascade_path)
    except IOError:
        die(2, 'Cascade classifier file not found')

    faces = get_faces(image, classifier)
    if len(faces) == 0:
        die(3, 'No faces detected')

    draw_clouds(image, faces)
    write_image(image, args.output_path)
